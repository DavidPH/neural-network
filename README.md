**Instructions for use:**
 - Initialize an instance of the model class `model = nn.Model()`
 - Use `model.add()` to add dense layers, drop out layers, and activation functions.
   - `nn.LayerDense(inputs:int, neurons:int)`
   - `nn.LayerDropout(rate:int)`
   - `nn.ActivationReLU()`
   - `nn.ActivationSoftmax()`
   - `nn.ActivationSigmoid()`
  - set the loss, accuracy, and optimizer
    - `nn.set(loss=..., accuracy=..., optimizer=...)`
    - Loss:
      - `nn.LossCategoricalCrossentropy()`
      - `nn.LossBinaryCrossentropy()`
      - `nn.LossMeanSquaredError()`
      - `nn.LossMeanAbsoluteError()`
    - Accuracy:
      - `nn.AccuracyRegression()`
      - `nn.AccuracyCategorical()`
     - Optimizer:
       - `nn.OptimizerSGD(learning_rate, decay, momentum)`
       - `nn.OptimizerAdagrad(learning_rate, decay, epsilon)`
       - `nn.OptimizerRMSprop(learning_rate, decay, epsilon, rho)`
       - `nn.OptimizerAdam(learning_rate, decay, epsilon, beta_1, beta_2)`
     -  `nn.finalize()` Must be called after using `nn.set()`
- Train the model or load saved parameters:
  - `model.train(X, y, epochs:int, validation_data:tuple, batch_size:int)`
  - `model.load_parameters(path:str)`

**Storing the nn to files**<br>
Use `model.save_parameters(path)` to save layer weights and biases to a file. You can the load them using `model.load_parameters(path)`. You must have a model initialized with the layers set for this to work.<br>
Alternatively you can save an entire model using `mode.save(path)` then you can initialize a new model using `model = nn.Model.load(path)`

**Examples of use:**<br>
https://gitlab.com/DavidPH/mnist-database-nn
